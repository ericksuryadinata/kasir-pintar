'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.post('/auth','WelcomeController.auth')
// I'm sorry, i don't know how to use auth:jwt from adonis, some documentation need database to do it
// So i must use own implementation to do this :)
Route.get('/', 'WelcomeController.index').middleware(['native'])
