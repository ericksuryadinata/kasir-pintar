'use strict'

const { validate } = use('Validator')
const jwt = use('jsonwebtoken')
const Config = use('Config')
const appKey = Config.get('app.appKey')
/**
 * Maybe the code is not standart enough because I don't know how to use beautifier
 */
class WelcomeController {
  /**
   *
   * @param {Response} ctx.response
   */
  async index ({response}){
    let messages = {
      'messages': 'KasirPintar, it can\'t accessed if no JWT token provided'
    }
    return response.json(messages)
  }

  /**
   *
   * @param {Request} ctx.Request
   * @param {Response} ctx.Response
   */
  async auth ({request, response, auth}){
    // simple constant data
    const EMAIL = 'admin@admin.com'
    const PASSWORD = 'password'

    // we will make validation first
    const validation = await validate(request.all(),{
      email : 'required',
      password : 'required'
    })

    if(validation.fails()){
      return response.send(validation.messages())
    }

    // take only email and password, I assume that user can send other data
    let user_input = request.only(['email', 'password'])

    if (user_input.email === EMAIL) {
      if (user_input.password === PASSWORD) {

        // if all provided
        const payload = {
          check : true
        }
        // create the jwt token
        let token = jwt.sign(payload, appKey, {
          expiresIn:1440
        })

        return response.json({
          "user": user_input.email,
          "token": token
        })

      } else {
        return response.json({
          message: 'Please Check Your Password'
        })
      }
    } else {
      return response.json({
        message: 'Please Input Correct Email'
      })
    }
  }
}

module.exports = WelcomeController
