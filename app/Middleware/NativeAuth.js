'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const jwt = use('jsonwebtoken')
const Config = use('Config')
const appKey = Config.get('app.appKey')

class NativeAuth {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Function} next
   */
  async handle ({ request, response }, next) {

    // get the token first
    let token = request.header('access-token') || request.header('x-access-token') || request.header('Authorization') || request.header('authorization')

    if(token){
      // token provided
      console.log("token provided")
      try {
        let decoded = jwt.verify(token, appKey);
        request.decoded = decoded
      } catch (err) {
        console.log("token provided but error")
        return response.status(400).json({
          status: 'error',
          message: 'Invalid Token'
        })
      }

    }else{
      // no token provided
      console.log("No token provided")
      return response.status(400).json({
        status : 'error',
        message : 'No Token Provided'
      })
    }
    await next()
  }
}

module.exports = NativeAuth
