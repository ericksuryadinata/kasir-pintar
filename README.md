# Kasir Pintar Assignment

## Quick Brief

I just got an email that I must create simple REST API using NodeJS, on that requirement whatever frameworks are welcome, but they refer to use adonisJS

Visiting adonisJS, read the installation guide, I found some interesting that adonis is same like laravel. Maybe I'll not as fast as when I use laravel, because I need to learn about the JS language it self :).

### Quick Install

```sh
npm install
```

then to use it

```sh
node server.js
```

or if you have adonis installed globally, you can use

```sh
adonis serve --dev
```

or if you have pm2 installed, you can use

```sh
pm2 start server.js
```

__NOTE :__

attach this to request body

email : `admin@admin.com`
password : `password`

you can use `access-token` or `x-access-token` or `Authorization` or `authorization` headers on next request

__ENDPOIINT :__

1. / method GET, with token
2. /auth method POST, with request body `email` and `password`

### Online API

you can use `https://kasirpintar.lungguh.pw` for online checking
